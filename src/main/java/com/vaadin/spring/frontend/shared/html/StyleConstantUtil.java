/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared.html;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public final class StyleConstantUtil {


    public static final String NON_SELECTABLE = "non-selectable";

    public static final String GWT_HTML = "gwt-HTML";

    public static final String GWT_LABEL = "gwt-Label";

    public static final String TRANSITIONED = "transitioned";

    public static final String SELECTED = "selected";

    public static final String HIGHLIGHTED = "highlighted";

    public static final String NODE = "node";

    public static final String CHILDREN = "children";

    public static final String WRAPPER_PANEL = "wrapperpanel";

    public static final String CHILD = "child";

    public static final String CONTENT = "content";

    public static final String HAS_ICON = "hasicon";

    public static final String ICON_LABEL = "iconlabel";

    public static final String TITLE_LABEL = "titlelabel";

    public static final String getComposedStyle(String... styles) {
        String composedStyle = "";
        for(String style: styles) {
            composedStyle += style + " ";
        }
        return composedStyle.length() > 0 ? composedStyle.substring(0, composedStyle.length() - 1) : "";
    }

}
