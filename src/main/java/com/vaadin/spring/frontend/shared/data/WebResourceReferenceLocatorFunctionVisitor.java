/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared.data;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface WebResourceReferenceLocatorFunctionVisitor<T extends WebResourceReferenceLocator>  {

    default
    void  visit(T  referenceLocator, Function<T, Void> referenceVisitorFunction) {
        Optional.of(referenceLocator).map(T::getRelationship).ifPresent(s -> {
            if (!s.equalsIgnoreCase("root")) {
                referenceVisitorFunction.apply(referenceLocator);
            }
            visitReferenceLocator(referenceLocator, referenceVisitorFunction);
        });
    }

    default
    void visitReferenceLocator(T referenceLocator, Function<T, Void> referenceVisitorFunction) {
        Optional.ofNullable(referenceLocator.getWebResourceReferences())
                .ifPresent(references -> ((Map<String, T>) references).
                        values().stream().forEach(reference -> { referenceVisitorFunction.apply(reference);
                    Optional.ofNullable(reference.getWebResourceReferences())
                            .ifPresent( resourceLocator -> ((Map<String, T>) resourceLocator).values().stream()
                                    .forEach(resource -> visit(resource, referenceVisitorFunction)));
        }));
    }

    Void visitReference(T resourceReference);

}
