/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared.html;

import com.vaadin.spring.frontend.shared.data.WebResource;
import com.vaadin.spring.frontend.shared.data.WebResourceDto;

import java.util.Optional;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public final class HtmlConstantUtil {


    public static String DIV_TAG = "div";



    public static void setContentIconHtml(WebResource webResource) {
        String html = elementWithClass(DIV_TAG,
                StyleConstantUtil.getComposedStyle(StyleConstantUtil.CONTENT,
                        StyleConstantUtil.TRANSITIONED, StyleConstantUtil.HAS_ICON));
        if(webResource.getId() != null && !webResource.getId().isEmpty()) {
            html = setElementId(html, webResource.getId());
        }
        html += elementWithClass(DIV_TAG,
                StyleConstantUtil.getComposedStyle(StyleConstantUtil.GWT_HTML,
                        StyleConstantUtil.ICON_LABEL, StyleConstantUtil.TRANSITIONED));
        html += "&#x" + Integer.toHexString(webResource.getCodepoint())+";";
        html += closeElement(DIV_TAG);
        html += elementWithClass(DIV_TAG,
                StyleConstantUtil.getComposedStyle(StyleConstantUtil.GWT_HTML,
                        StyleConstantUtil.TITLE_LABEL, StyleConstantUtil.TRANSITIONED));
        html += Optional.ofNullable(webResource.getTitle()).orElse("");
        html += closeElement(DIV_TAG);
        ((WebResourceDto)webResource).setHtml(html + closeElement(DIV_TAG));
    }

    public static String closeElement(String element) {
        return "</"+element+">";
    }

    public static String elementWithClass(String element, String style) {
        return "<"+element+" class=\""+style+"\">";
    }

    public static String setElementId(String element, String id) {
        return element.substring(0, element.indexOf(" "))+" id=\""+id+"\""+element.substring(element.indexOf(" "));
    }

}
