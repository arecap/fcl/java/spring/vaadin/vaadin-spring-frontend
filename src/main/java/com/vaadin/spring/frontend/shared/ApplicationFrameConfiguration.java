/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared;

import elemental.json.JsonObject;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public interface ApplicationFrameConfiguration extends JsonObject {

    String JWT_KEY = "jwtKey";

    String BROWSER_DETAILS_URL = "browserDetailsUrl";

    String CONTEXT_ROOT_URL = "contextRootUrl";

    String FRONTEND_URL = "frontendUrl";

    String SERVICE_URL = "serviceUrl";

    String WIDGETSET = "widgetset";

    String THEME = "theme";

    String VERSION_INFO = "versionInfo";

    String VAADIN_DIR = "vaadinDir";

    String HEARTBEAT_INTERVAL = "heartbeatInterval";

    String DEBUG = "debug";

    String STANDALONE = "standalone";

    String MESSAGE = "message";

    String CAPTION = "caption";

    String URL = "caption";

    String AUTH_ERR_MSG = "authErrMsg";

    String COM_ERR_MSG = "comErrMsg";

    String SESS_ERR_MSG = "sessExpMsg";

    String SEND_URLS_AS_PARAMETERS = "sendUrlsAsParameters";

    default String getJwtKey() {
        return getString(JWT_KEY);
    }

    default void setJwtKey(String jwtKey) {
        put(JWT_KEY, jwtKey);
    }

    default String getBrowserDetailsUrl() {
        return getString(BROWSER_DETAILS_URL);
    }

    default void setBrowserDetailsUrl(String browserDetailsUrl) {
        put(BROWSER_DETAILS_URL, browserDetailsUrl);
    }

    default String getContextRootUrl() {
        return getString(CONTEXT_ROOT_URL);
    }

    default void setContextRootUrl(String contextRootUrl) {
        put(CONTEXT_ROOT_URL, contextRootUrl);
    }

    default String getFrontendUrl() {
        return getString(FRONTEND_URL);
    }

    default void setFrontendUrl(String frontendUrl) {
        put(FRONTEND_URL, frontendUrl);
    }

    default String getServiceUrl() {
        return getString(SERVICE_URL);
    }

    default void setServiceUrl(String serviceUrl) {
        put(SERVICE_URL, serviceUrl);
    }

    default String getWidgetset() {
        return getString(WIDGETSET);
    }

    default void setWidgetset(String widgetset) {
        put(WIDGETSET, widgetset);
    }

    default String getTheme() {
        return getString(THEME);
    }

    default void setTheme(String theme) {
        put(THEME, theme);
    }

    default JsonObject getVersionInfo() {
        return get(VERSION_INFO);
    }

    default void setVersionInfo(JsonObject versionInfo) {
        put(VERSION_INFO, versionInfo);
    }

    default String getVaadinDir() {
        return get(VAADIN_DIR);
    }

    default void setVaadinDir(String vaadinDir) {
        put(VAADIN_DIR, vaadinDir);
    }

    default int getHeartbeatInterval() {
        return (int) getNumber(HEARTBEAT_INTERVAL);
    }

    default void setHeartbeatInterval(int interval) {
        put(HEARTBEAT_INTERVAL, interval);
    }

    default boolean isDebug() {
        return getBoolean(DEBUG);
    }

    default void setDebug(boolean debug) {
        put(DEBUG, debug);
    }

    default boolean isStandalone() {
        return getBoolean(STANDALONE);
    }

    default void setStandalone(boolean standalone) {
        put(STANDALONE, standalone);
    }

    default JsonObject getAuthErrMessage() {
        return get(AUTH_ERR_MSG);
    }

    default void setAuthErrMsg(JsonObject authErrMsg) {
        put(AUTH_ERR_MSG, authErrMsg);
    }

    default JsonObject getComErrMsg() {
        return get(COM_ERR_MSG);
    }

    default void setComErrMsg(JsonObject comErrMsg) {
        put(COM_ERR_MSG, comErrMsg);
    }

    default JsonObject getSessErrMsg() {
        return get(SESS_ERR_MSG);
    }

    default void setSessErrMsg(JsonObject sessErrMsg) {
        put(SESS_ERR_MSG, sessErrMsg);
    }

    default boolean isSendUrlsAsParameters() {
        return getBoolean(SEND_URLS_AS_PARAMETERS);
    }

    default void setSendUrlsAsParameterse(boolean sendUrlsAsParameters) {
        put(SEND_URLS_AS_PARAMETERS, sendUrlsAsParameters);
    }

}
