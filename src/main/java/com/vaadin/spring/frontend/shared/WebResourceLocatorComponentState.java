/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared;

import com.vaadin.shared.AbstractComponentState;
import com.vaadin.spring.frontend.shared.data.WebResourceReferenceDto;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class WebResourceLocatorComponentState extends AbstractComponentState  {

    //TODO logger

    public List<WebResourceReferenceDto>  webResourceReferences = new LinkedList<>();

//    public void setWebResourceReference(WebResourceReference webResourceReference) {
////        List<WebResourceReference> setupWebResources = webResourceReferences.stream()
////                .filter(ref -> ref.getId().equalsIgnoreCase(webResourceReference.getId())).collect(toList());
////        assert setupWebResources.size() <= 0;
////        setupWebResources.forEach( ref -> webResourceReferences.remove(ref));
//        webResourceReferences.add(webResourceReference);
//    }
//
////    public WebResourceReference getWebResourceReference(String id) {
////        for(WebResourceReference ref: webResourceReferences) {
////            if(ref.getId().equalsIgnoreCase(id)) {
////                return ref;
////            }
////        }
//////        List<WebResourceReference> setupWebResources = webResourceReferences.stream()
//////                .filter(ref -> ref.getId().equalsIgnoreCase(id)).collect(toList());
//////        assert setupWebResources.size() <= 0;
//////        return setupWebResources.size() == 1 ? setupWebResources.get(0) : null;
////        return null;
////    }
//
//    @Override
//    public Iterable<WebResourceReference> getWebResourceReferences() {
//        return webResourceReferences;
//    }

}
