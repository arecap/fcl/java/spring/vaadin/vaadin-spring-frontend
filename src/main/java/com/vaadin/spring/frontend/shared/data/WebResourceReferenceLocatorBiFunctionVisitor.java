/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared.data;

import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

/**
 *
 *
 * @author Octavian Stirbei
 * @since 1.0
 */
public interface WebResourceReferenceLocatorBiFunctionVisitor<S extends WebResourceReferenceLocator, T>  {

    default
    void visit(S referenceLocator, T input, BiFunction<T, S, T> referenceVisitorBiFunction) {
        Optional.of(referenceLocator).map(S::getRelationship)
                .ifPresent( s -> visitReferenceLocator(referenceLocator,
                        s.equalsIgnoreCase("root") ?
                                input : referenceVisitorBiFunction.apply(input, referenceLocator),
                        referenceVisitorBiFunction));
    }

    default
    void visitReferenceLocator(S referenceLocator, T input, BiFunction<T, S, T> referenceVisitorBiFunction) {
        Optional.ofNullable(referenceLocator.getWebResourceReferences()).ifPresent( references -> {
            ((Map<String, S>)references).values().stream()
                    .forEach(reference -> {
                        T output = referenceVisitorBiFunction.apply(input, reference);
                        Optional.ofNullable(reference.getWebResourceReferences()).ifPresent( resourceLocator -> {
                            ((Map<String, S>)resourceLocator).values().stream()
                                    .forEach( resource -> visit(resource, output, referenceVisitorBiFunction));
                        });
                    });
        });
    }

    T visitReference(S resourceReference, T input);

}
