/*
 * Copyright 2017-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.shared;

import com.vaadin.shared.communication.URLReference;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class BreadcrumbReference {


    private String caption;

    private URLReference urlReference;

    public BreadcrumbReference() {
    }

    public BreadcrumbReference(String caption, URLReference urlReference) {
        this.caption = caption;
        this.urlReference = urlReference;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public URLReference getUrlReference() {
        return urlReference;
    }

    public void setUrlReference(URLReference urlReference) {
        this.urlReference = urlReference;
    }

}
