/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.ui;


import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.*;
import com.vaadin.server.communication.AtmospherePushConnection;
import com.vaadin.shared.Version;
import com.vaadin.spring.endpoint.annotation.GuiController;
import com.vaadin.spring.frontend.shared.ApplicationConfigurationFactory;
import com.vaadin.spring.frontend.shared.ApplicationFrameConfiguration;
import com.vaadin.ui.UI;
import elemental.json.Json;
import elemental.json.JsonObject;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.annotation.AnnotationUtils;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public final class ApplicationFrameUtil {


    public static ApplicationFrameConfiguration createApplicationFrameConfiguration(Class<?> controllerType) {
        AnnotationAttributes aa = AnnotatedElementUtils
                .getMergedAnnotationAttributes(controllerType, GuiController.class);

//        GuiController guiController = AnnotationUtils.findAnnotation(controllerType, GuiController.class);
        assert aa != null;
        Class<? extends UI> uiType = aa.getClass("ui");
        Theme theme = AnnotationUtils.findAnnotation(uiType, Theme.class);
        Widgetset widgetset = AnnotationUtils.findAnnotation(uiType, Widgetset.class);
        ApplicationFrameConfiguration applicationFrameConfiguration = createSystemApplicationFrameConfiguration();
        applicationFrameConfiguration.setTheme(theme == null ? "valo" : theme.value());
        applicationFrameConfiguration.setWidgetset(widgetset == null ? "com.vaadin.DefaultWidgetSet" : widgetset.value());
        applicationFrameConfiguration.setBrowserDetailsUrl(aa.getString("uri"));
        return applicationFrameConfiguration;
    }

    public static ApplicationFrameConfiguration createApplicationFrameConfiguration(String contextRootUrl, String controllerUri, String theme, String widgetSet) {
        ApplicationFrameConfiguration applicationFrameConfiguration = createSystemApplicationFrameConfiguration();
        applicationFrameConfiguration.setTheme(theme == null ? "valo" : theme);
        applicationFrameConfiguration.setWidgetset(widgetSet == null ? "com.vaadin.spring.frontend.FrontendWidgetSet" : widgetSet);
        applicationFrameConfiguration.setContextRootUrl(contextRootUrl);
        applicationFrameConfiguration.setBrowserDetailsUrl(controllerUri);
        return applicationFrameConfiguration;
    }


    public static ApplicationFrameConfiguration createSystemApplicationFrameConfiguration() {
        VaadinService service = VaadinService.getCurrent();
        VaadinRequest request = VaadinService.getCurrentRequest();
        VaadinSession session = VaadinSession.getCurrent();
        ApplicationFrameConfiguration applicationFrameConfiguration = setupSystemMessages(new ApplicationConfigurationFactory().create());
        applicationFrameConfiguration.setVersionInfo(getVersionInfoObject());
        applicationFrameConfiguration
                .setContextRootUrl(VaadinServletService.getContextRootRelativePath(request));
        applicationFrameConfiguration.setVaadinDir(service.getStaticFileLocation(request)
                + "/VAADIN/");
        applicationFrameConfiguration.setFrontendUrl(BootstrapHandler.resolveFrontendUrl(session));
        applicationFrameConfiguration.setDebug(!session.getConfiguration().isProductionMode());
        applicationFrameConfiguration.setStandalone(service.isStandalone(request));
        applicationFrameConfiguration.setHeartbeatInterval(service
                .getDeploymentConfiguration().getHeartbeatInterval());
        applicationFrameConfiguration.setServiceUrl("/vaadinServlet");
        applicationFrameConfiguration.setSendUrlsAsParameterse(service.getDeploymentConfiguration().isSendUrlsAsParameters());
        return applicationFrameConfiguration;
    }


    public static JsonObject getVersionInfoObject() {
        JsonObject versionInfo = Json.createObject();
        versionInfo.put("vaadinVersion",Version.getFullVersion());
        String atmosphereVersion = AtmospherePushConnection
                .getAtmosphereVersion();
        if (atmosphereVersion != null) {
            versionInfo.put("atmosphereVersion", atmosphereVersion);
        }
        return versionInfo;
    }

    public static ApplicationFrameConfiguration setupSystemMessages(ApplicationFrameConfiguration applicationFrameConfiguration) {
        SystemMessages systemMessages = VaadinService.getCurrent().getSystemMessages(UI.getCurrent().getLocale(), VaadinService.getCurrentRequest());
        if(systemMessages != null) {
            applicationFrameConfiguration.setAuthErrMsg(newJsonMessage(systemMessages.getAuthenticationErrorCaption(),
                    systemMessages.getAuthenticationErrorMessage(), systemMessages.getAuthenticationErrorURL()));
            applicationFrameConfiguration.setComErrMsg(newJsonMessage(systemMessages.getCommunicationErrorCaption(),
                    systemMessages.getCommunicationErrorMessage(), systemMessages.getCommunicationErrorURL()));
            applicationFrameConfiguration.setSessErrMsg(newJsonMessage(systemMessages.getSessionExpiredCaption(),
                    systemMessages.getSessionExpiredMessage(), systemMessages.getSessionExpiredURL()));
        }
        return applicationFrameConfiguration;
    }


    public static JsonObject newJsonMessage(String caption, String msg, String url) {
        JsonObject jsonMsg = Json.createObject();
        if(caption == null)
            jsonMsg.put(ApplicationFrameConfiguration.CAPTION, Json.createNull());
        else
            jsonMsg.put(ApplicationFrameConfiguration.CAPTION, caption);
        if(msg == null)
            jsonMsg.put(ApplicationFrameConfiguration.MESSAGE, Json.createNull());
        else
            jsonMsg.put(ApplicationFrameConfiguration.MESSAGE, msg);
        if(url == null)
            jsonMsg.put(ApplicationFrameConfiguration.URL, Json.createNull());
        else
            jsonMsg.put(ApplicationFrameConfiguration.URL, url);
        return jsonMsg;
    }

    public static String applicationInitExecuteScript(String applicationFrameId, ApplicationFrameConfiguration applicationFrameConfiguration) {
        String appInitExecutionScript = "vaadin.initApplication(\'" + applicationFrameId + "\', " + applicationFrameConfiguration.toJson() + ");";
        return appInitExecutionScript;
    }

}
