/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.ui;


import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.shared.communication.URLReference;
import com.vaadin.spring.endpoint.util.NavigatorUtils;
import com.vaadin.spring.frontend.shared.BreadcrumbReference;
import com.vaadin.spring.frontend.shared.BreadcrumbServerRpc;
import com.vaadin.spring.frontend.shared.BreadcrumbState;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.UI;
import org.springframework.context.MessageSource;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class Breadcrumb extends AbstractComponent {

    private BreadcrumbServerRpc rpc = new BreadcrumbServerRpc() {
        @Override
        public void click(String breadcrumb) {
            navigateTo(breadcrumb);
        }
    };

//    private BreadcrumbServerRpc rpc = (BreadcrumbServerRpc) breadcrumb -> navigateTo(breadcrumb);

    public Breadcrumb() {
        registerRpc(rpc);
    }

    public void addBreadcrumb(String caption, Class<?> controllerType, Object... params) {
        URLReference urlReference = new URLReference();
        urlReference.setURL(NavigatorUtils.getControllerUrl(controllerType, params));
        getState().breadcrumbReferences.add(new BreadcrumbReference(caption, urlReference));
    }

    public void addBreadcrumb(MessageSource messageSource, String caption, Class<?> controllerType, Object... params) {
        assert messageSource != null;
        addBreadcrumb(messageSource.getMessage(caption, null, caption, UI.getCurrent().getLocale()), controllerType, params);
    }

    public void addBreadcrumb(String caption, Resource resource) {
        assert resource.getClass().isAssignableFrom(ExternalResource.class);
        URLReference urlReference = new URLReference();
        urlReference.setURL(((ExternalResource) resource).getURL());
        getState().breadcrumbReferences.add(new BreadcrumbReference(caption, urlReference));
    }

    public void addBreadcrumb(MessageSource messageSource, String caption, Resource resource) {
        assert messageSource != null;
        addBreadcrumb(messageSource.getMessage(caption, null, caption, UI.getCurrent().getLocale()), resource);
    }

    protected void navigateTo(String breadcrumb) {
        for(BreadcrumbReference breadcrumbReference: getState().breadcrumbReferences) {
            if(breadcrumbReference.getCaption().equalsIgnoreCase(breadcrumb)) {
                UI.getCurrent().getPage().setLocation(breadcrumbReference.getUrlReference().getURL());
            }
        }
    }

    @Override
    protected BreadcrumbState getState() {
        return (BreadcrumbState) super.getState();
    }

    @Override
    protected BreadcrumbState getState(boolean markAsDirty) {
        return (BreadcrumbState) super.getState(markAsDirty);
    }


}
