/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.ui;


import com.vaadin.spring.frontend.shared.ApplicationFrameConfiguration;
import com.vaadin.spring.frontend.shared.ApplicationFrameState;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.JavaScript;
import elemental.json.Json;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class ApplicationFrame extends AbstractComponent {

    private ApplicationFrameConfiguration applicationFrameConfiguration;

    public ApplicationFrame(ApplicationFrameConfiguration applicationFrameConfiguration) {
        this.applicationFrameConfiguration = applicationFrameConfiguration;
    }

    public ApplicationFrame(Class<?> controllerType) {
       this(ApplicationFrameUtil.createApplicationFrameConfiguration(controllerType));
    }

    public ApplicationFrame(String contextUrl, String controllerUri, String theme, String widgetSet) {
        this(ApplicationFrameUtil.createApplicationFrameConfiguration(contextUrl, controllerUri, theme, widgetSet));
    }

    public ApplicationFrame(String contextUrl, String controllerUri, String theme) {
        this(contextUrl, controllerUri, theme, null);
    }

    public ApplicationFrame(String contextUrl, String controllerUri) {
        this(contextUrl, controllerUri, null, null);
    }

    @Override
    public void setId(String id) {
        super.setId(id);
        getState().setApplicationFrameConfiguration(Json.parse(applicationFrameConfiguration.toJson()));
        initApplication(applicationFrameConfiguration);
    }

    private void initApplication(ApplicationFrameConfiguration applicationFrameConfiguration) {
        JavaScript.getCurrent().execute(ApplicationFrameUtil.applicationInitExecuteScript(getId(), applicationFrameConfiguration));
    }

    @Override
    protected ApplicationFrameState getState() {
        return (ApplicationFrameState) super.getState();
    }

    @Override
    protected ApplicationFrameState getState(boolean markAsDirty) {
        return (ApplicationFrameState) super.getState(markAsDirty);
    }

}
