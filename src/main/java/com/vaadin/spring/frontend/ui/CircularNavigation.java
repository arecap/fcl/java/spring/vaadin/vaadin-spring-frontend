/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.ui;


import com.vaadin.server.FontIcon;
import com.vaadin.spring.frontend.shared.CircularNavigationServerRpc;
import com.vaadin.spring.frontend.shared.CircularNavigationState;
import com.vaadin.spring.frontend.shared.data.WebResourceReference;
import com.vaadin.spring.frontend.shared.data.WebResourceReferenceDto;
import com.vaadin.spring.frontend.shared.html.HtmlConstantUtil;
import com.vaadin.ui.AbstractComponent;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class CircularNavigation extends AbstractComponent implements DefaultNavigationServerRpc  {



    public CircularNavigation() {
        registerRpc((CircularNavigationServerRpc) ref -> navigateTo(ref));
    }

    public void setContentIcon(FontIcon icon) {
        WebResourceReferenceDto ref = new WebResourceReferenceDto();
        ref.setRelationship("central-content-icon");
        ref.setFontFamily(icon.getFontFamily());
        ref.setCodepoint(icon.getCodepoint());
        HtmlConstantUtil.setContentIconHtml(ref);
        getState().webResourceReferences.add(ref);
    }

    public void addNavigationIcon(WebResourceReference ref) {
        getState().webResourceReferences.add((WebResourceReferenceDto) ref);
    }

    @Override
    protected CircularNavigationState getState() {
        return (CircularNavigationState) super.getState();
    }

    @Override
    protected CircularNavigationState getState(boolean markAsDirty) {
        return (CircularNavigationState) super.getState(markAsDirty);
    }

}
