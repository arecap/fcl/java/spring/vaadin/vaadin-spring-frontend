/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.StyleConstants;
import elemental.json.JsonObject;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class ApplicationFrameWidget extends Widget {

    protected Element altElement;
    protected String altText;

    public static final String CLASSNAME = "v-app";

    public ApplicationFrameWidget() {
        Element appFrame = Document.get().createDivElement();
        Element loadingFrame = Document.get().createDivElement();
        loadingFrame.setClassName("v-app-loading");
        appFrame.appendChild(loadingFrame);
        Element noScriptFrame = Document.get().createElement("noscript");
        noScriptFrame.setInnerText("You have to enable javascript in your browser to use an application built with Spring Vaadin.");
        appFrame.appendChild(noScriptFrame);
        setElement(appFrame);
        setStyleName(CLASSNAME);
    }


    public void setApplicationConnection(JsonObject applicationConfiguration) {
        removeStyleName(StyleConstants.UI_WIDGET);
    }

}
