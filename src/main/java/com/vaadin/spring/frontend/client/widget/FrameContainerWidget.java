/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.Profiler;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class FrameContainerWidget extends FlowPanel  {

    private static boolean hasInnerHtml = false;

    private static boolean hasInnerText = false;

    public FrameContainerWidget() {
        setElement(Document.get().createDivElement());
    }

    public void setAttribute(String name, String value) {
        getElement().setAttribute(name, value);
    }

    public void setInnerHtml(String html) {
        hasInnerHtml = true;
        if(hasInnerText) {
            getElement().setInnerHTML(html+getElement().getInnerText());
        } else {
            getElement().setInnerHTML(html);
        }
    }

    public void setInnerText(String text) {
        hasInnerText = true;
        if(hasInnerHtml) {
            getElement().setInnerHTML(getElement().getInnerHTML()+text);
        } else {
            getElement().setInnerText(text);
        }
    }

    /**
     * For internal use only. May be removed or replaced in the future.
     */
    public void addOrMove(Widget child, int index) {
        Profiler.enter("FrameContainerWidget.addOrMove");
        if (child.getParent() == this) {
            Profiler.enter("FrameContainerWidget.addOrMove getWidgetIndex");
            int currentIndex = getWidgetIndex(child);
            Profiler.leave("FrameContainerWidget.addOrMove getWidgetIndex");
            if (index == currentIndex) {
                Profiler.leave("FrameContainerWidget.addOrMove");
                return;
            }
        } else if (index == getWidgetCount()) {
            // optimized path for appending components - faster especially for
            // initial rendering
            Profiler.enter("FrameContainerWidget.addOrMove add");
            add(child);
            Profiler.leave("FrameContainerWidget.addOrMove add");
            Profiler.leave("FrameContainerWidget.addOrMove");
            return;
        }
        Profiler.enter("FrameContainerWidget.addOrMove insert");
        insert(child, index);
        Profiler.leave("FrameContainerWidget.addOrMove insert");
        Profiler.leave("FrameContainerWidget.addOrMove");
    }

}
