/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.ui;

import com.google.gwt.dom.client.Element;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.shared.ui.Connect;
import com.vaadin.spring.frontend.client.widget.CircularNavigationWidget;
import com.vaadin.spring.frontend.shared.CircularNavigationServerRpc;
import com.vaadin.spring.frontend.shared.CircularNavigationState;
import com.vaadin.spring.frontend.shared.data.WebResourceReference;
import com.vaadin.spring.frontend.ui.CircularNavigation;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Connect(CircularNavigation.class)
public class CircularNavigationConnector extends AbstractComponentConnector {

    public CircularNavigationConnector() {
        getWidget().addClickHandler(event -> {
            Element webResourceReferenceElementCandidate = Element.as(event.getNativeEvent().getEventTarget());
            if(webResourceReferenceElementCandidate.getTagName().equalsIgnoreCase("div")
                    && webResourceReferenceElementCandidate.getAttribute("rel").isEmpty()) {
                CircularNavigationServerRpc rpc = getRpcProxy(CircularNavigationServerRpc.class);
                getState().webResourceReferences.forEach(ref ->
                {
                    if(ref.getId().equalsIgnoreCase(webResourceReferenceElementCandidate.getAttribute("rel"))){
                        rpc.navigateTo(ref);
                    }});
//                rpc.navigateTo(getState()
//                        .getWebResourceReference(webResourceReferenceElementCandidate.getAttribute("rel")));
            }
        });
    }

    @Override
    public CircularNavigationWidget getWidget() {
        return (CircularNavigationWidget) super.getWidget();
    }

    @Override
    public CircularNavigationState getState() {
        return (CircularNavigationState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        for(WebResourceReference ref: getState().webResourceReferences) {
            if(ref.getRelationship().equalsIgnoreCase("central-content-icon")) {
                getWidget().setContentIcon(ref);
                continue;
            }
            getWidget().setNavigationIcon(ref);
        }
        getWidget().updateFontSize();
    }

}
