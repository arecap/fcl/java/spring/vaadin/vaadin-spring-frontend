/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.ui;

import com.google.gwt.dom.client.Element;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.shared.ui.Connect;
import com.vaadin.spring.frontend.client.widget.BreadcrumbWidget;
import com.vaadin.spring.frontend.shared.BreadcrumbServerRpc;
import com.vaadin.spring.frontend.shared.BreadcrumbState;
import com.vaadin.spring.frontend.ui.Breadcrumb;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Connect(Breadcrumb.class)
public class BreadcrumbConnector extends AbstractComponentConnector {

    public BreadcrumbConnector() {
        getWidget().addClickHandler(event -> {
            Element actionElement = Element.as(event.getNativeEvent().getEventTarget());
            if(actionElement.getTagName().equalsIgnoreCase("div")
                    && actionElement.getClassName().contains("crumb")) {
                BreadcrumbServerRpc rpc = getRpcProxy(BreadcrumbServerRpc.class);
                rpc.click(actionElement.getInnerText());
            }
        });

    }

    @Override
    public BreadcrumbWidget getWidget() {
        return (BreadcrumbWidget) super.getWidget();
    }

    @Override
    public BreadcrumbState getState() {
        return (BreadcrumbState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        for(int i = 0; i < getState().breadcrumbReferences.size() - 1; i++) {
            getWidget().addCrumbNext(getState().breadcrumbReferences.get(i).getCaption());
        }
        if(getState().breadcrumbReferences.size() > 0) {
            getWidget().addCrumb(getState().breadcrumbReferences.get(getState().breadcrumbReferences.size() - 1).getCaption());
        }
    }

}
