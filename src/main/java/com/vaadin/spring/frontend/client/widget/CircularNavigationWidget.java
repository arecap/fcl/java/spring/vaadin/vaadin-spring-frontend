/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FocusWidget;
import com.vaadin.spring.frontend.shared.data.WebResourceReference;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class CircularNavigationWidget extends FocusWidget implements ResizeHandler {

    private final static Element navigationRoot = Document.get().createDivElement();

    private final static Element navigationWrapperPanel = createWrapperTransitionedElement();

    private final static Element children = navigationWrapperPanel.getFirstChildElement();

    private static Element createWrapperTransitionedElement() {
        Element wrapped = Document.get().createDivElement();
        wrapped.addClassName(StyleConstantUtil
                .getComposedStyle(StyleConstantUtil.WRAPPER_PANEL, StyleConstantUtil.TRANSITIONED ));
        Element children = Document.get().createDivElement();
        children.addClassName(StyleConstantUtil.getComposedStyle(StyleConstantUtil.CHILDREN,
                StyleConstantUtil.TRANSITIONED));
        children.setAttribute("style", "transform: rotate(0rad) translateX(0em) translateY(0em);");
        wrapped.appendChild(children);
        return wrapped;
    }

    public CircularNavigationWidget() {
        setElement(Document.get().createDivElement());
        navigationRoot.appendChild(navigationWrapperPanel);
        getElement().appendChild(navigationRoot);
        setStyleName(StyleConstantUtil.
                getComposedStyle(StyleConstantUtil.TRANSITIONED, StyleConstantUtil.NON_SELECTABLE));
        navigationRoot.addClassName(StyleConstantUtil
                .getComposedStyle(StyleConstantUtil.NODE, StyleConstantUtil.TRANSITIONED,
                        StyleConstantUtil.SELECTED, StyleConstantUtil.HIGHLIGHTED));
    }

    public void setContentIcon(WebResourceReference ref) {
        Element contentIcon = Document.get().createDivElement();
        contentIcon.setInnerHTML(ref.getHtml());
        navigationWrapperPanel.appendChild(contentIcon.getFirstChild());
    }

    public void setNavigationIcon(WebResourceReference ref) {
        Element contentIcon = Document.get().createDivElement();
        contentIcon.addClassName(StyleConstantUtil
                .getComposedStyle(StyleConstantUtil.NODE, StyleConstantUtil.TRANSITIONED, StyleConstantUtil.CHILD));
        Element wrapped = createWrapperTransitionedElement();
        contentIcon.appendChild(wrapped);
        wrapped.setInnerHTML(ref.getHtml());
        children.appendChild(contentIcon);
        updateStyleTransform();
    }

    public void updateFontSize() {
        getElement().setAttribute("style", "font-size: "+ Math.min(Window.getClientWidth(), Window.getClientHeight())+"px");
    }

    protected void updateStyleTransform() {
        double angle = 0;
        for(int i = 0; i < children.getChildCount(); i++) {
            double top = zeroIfLess(Math.cos(angle)) * (-50);
            double left = zeroIfLess(Math.sin(angle)) * 50;
            angle += (2 * Math.PI) / children.getChildCount();
            ((Element)children.getChild(i)).setAttribute("style",
                    "left: " + formatNf(left, 2)
                            +"%; top: " + formatNf(top, 2)
                            + "%; transform: rotate(0rad) translateX("
                            + formatNf(left / 100, 2)
                            + "em) translateY(" + formatNf(top / 100, 2)
                            + "em);");
        }
    }

    private double zeroIfLess(double val) {
        return Math.abs(val) < 1.0E-3 ? 0.0 : val;
    }

    private String formatNf(double val, int n) {
        String strVal = val + "";
        return strVal.indexOf(".") == -1 ? strVal : strVal.substring(0, strVal.indexOf(".") + n + 1);
    }

    @Override
    public void onResize(ResizeEvent event) {
        updateFontSize();
    }

}
