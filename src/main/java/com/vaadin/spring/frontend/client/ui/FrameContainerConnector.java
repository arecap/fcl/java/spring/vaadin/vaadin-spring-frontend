/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.ui;

import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.*;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentContainerConnector;
import com.vaadin.client.ui.ClickEventHandler;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.Connect;
import com.vaadin.spring.frontend.client.widget.FrameContainerWidget;
import com.vaadin.spring.frontend.shared.FrameContainerState;
import com.vaadin.spring.frontend.shared.FrameContainerUpdateServerRpc;
import com.vaadin.spring.frontend.ui.FrameContainer;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
@Connect(FrameContainer.class)
public class FrameContainerConnector extends AbstractComponentContainerConnector {

//    private LayoutClickEventHandler clickEventHandler = new LayoutClickEventHandler(
//            this) {
//
//        @Override
//        protected ComponentConnector getChildComponent(
//                com.google.gwt.user.client.Element element) {
//            return Util.getConnectorForElement(getConnection(), getWidget(),
//                    element);
//        }
//
//        @Override
//        protected LayoutClickRpc getLayoutClickRPC() {
//            return getRpcProxy(FrameContainerServerRpc.class);
//        }
//    };


    private ClickEventHandler clickEventHandler = new ClickEventHandler(this) {
        @Override
        protected void fireClick(NativeEvent event, MouseEventDetails mouseDetails) {
            if(mouseDetails.getButton() == MouseEventDetails.MouseButton.LEFT) {
                FrameContainerUpdateServerRpc rpc = getRpcProxy(FrameContainerUpdateServerRpc.class);
                rpc.click(mouseDetails);
            }
            event.stopPropagation();
        }
    };

    public FrameContainerConnector() {
        Window.addResizeHandler(event -> {
            FrameContainerUpdateServerRpc rpc = getRpcProxy(FrameContainerUpdateServerRpc.class);
            rpc.onStateUpdate();
        });

    }

    private final FastStringMap<VCaption> childIdToCaption = FastStringMap
            .create();

    @Override
    public FrameContainerWidget getWidget() {
        return (FrameContainerWidget) super.getWidget();
    }

    @Override
    public FrameContainerState getState() {
        return (FrameContainerState) super.getState();
    }

    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);
        clickEventHandler.handleEventHandlerRegistration();
        if(getState().removeFrameworkStyle) {
            getWidget().removeStyleName(StyleConstants.UI_WIDGET);
        }
        for(String attributeName: getState().attributes.keySet()) {
            getWidget().setAttribute(attributeName, getState().attributes.get(attributeName));
        }
        if(getState().innerText != null && !getState().innerText.isEmpty()) {
            getWidget().setInnerText(getState().innerText);
        }
        if(getState().innerHtml != null && !getState().innerHtml.isEmpty()) {
            getWidget().setInnerHtml(getState().innerHtml);
        }
    }

    @Override
    public void onConnectorHierarchyChange(ConnectorHierarchyChangeEvent event) {
        Profiler.enter("FrameContainerConnector.onConnectorHierarchyChange");

        // Detach old child widgets and possibly their caption
        Profiler.enter(
                "FrameContainerConnector.onConnectorHierarchyChange remove old children");
        for (ComponentConnector child : event.getOldChildren()) {
            if (child.getParent() == this) {
                // Skip current children
                continue;
            }
            getWidget().remove(child.getWidget());
            VCaption vCaption = childIdToCaption.get(child.getConnectorId());
            if (vCaption != null) {
                childIdToCaption.remove(child.getConnectorId());
                getWidget().remove(vCaption);
            }
        }
        Profiler.leave(
                "FrameContainerConnector.onConnectorHierarchyChange remove old children");

        Profiler.enter(
                "FrameContainerConnector.onConnectorHierarchyChange add children");
        int index = 0;
        for (ComponentConnector child : getChildComponents()) {
            VCaption childCaption = childIdToCaption
                    .get(child.getConnectorId());
            if (childCaption != null) {
                getWidget().addOrMove(childCaption, index++);
            }
            getWidget().addOrMove(child.getWidget(), index++);
        }
        Profiler.leave(
                "FrameContainerConnector.onConnectorHierarchyChange add children");

        Profiler.leave("FrameContainerConnector.onConnectorHierarchyChange");

    }

    @Override
    public void updateCaption(ComponentConnector child) {
        Widget childWidget = child.getWidget();
        int widgetPosition = getWidget().getWidgetIndex(childWidget);

        String childId = child.getConnectorId();
        VCaption caption = childIdToCaption.get(childId);
        if (VCaption.isNeeded(child)) {
            if (caption == null) {
                caption = new VCaption(child, getConnection());
                childIdToCaption.put(childId, caption);
            }
            if (!caption.isAttached()) {
                // Insert caption at widget index == before widget
                getWidget().insert(caption, widgetPosition);
            }
            caption.updateCaption();
        } else if (caption != null) {
            childIdToCaption.remove(childId);
            getWidget().remove(caption);
        }
    }

}
