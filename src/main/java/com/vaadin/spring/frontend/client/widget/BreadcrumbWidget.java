/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.FocusWidget;
import com.vaadin.spring.frontend.shared.html.StyleConstantUtil;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public class BreadcrumbWidget extends FocusWidget  {


    private Element breadcrumbTableRow = Document.get().createTRElement();

    public BreadcrumbWidget() {
        Element breadcrumbTable = Document.get().createTableElement();
        breadcrumbTable.setAttribute("cellspacing", "0");
        breadcrumbTable.setAttribute("cellpadding", "0");
        Element breadcrumbTableBody = Document.get().createTBodyElement();
        breadcrumbTableBody.appendChild(breadcrumbTableRow);
        breadcrumbTable.appendChild(breadcrumbTableBody);
        addLogoWrapper();
        setElement(breadcrumbTable);
        setStyleName(StyleConstantUtil.NON_SELECTABLE);
    }

    public void addCrumb(String text) {
        Element gwtLabel = createGwtLabel();
        gwtLabel.addClassName("crumb");
        gwtLabel.setInnerText(text);
        addCell(gwtLabel);
    }

    public void addCrumbNext(String text) {
        addCrumb(text);
        addSpacer();
    }

    private void addSpacer() {
        Element gwtLabel = createGwtLabel();
        gwtLabel.addClassName("spacer");
        gwtLabel.setInnerText(">");
        addCell(gwtLabel);
    }

    private void addCell(Element crumb) {
        Element crumbCell = createCell();
        crumbCell.appendChild(crumb);
        breadcrumbTableRow.appendChild(crumbCell);
    }

    private void addLogoWrapper() {
        Element logoWrapper = Document.get().createDivElement();
        logoWrapper.addClassName("logowrapper");
        Element gwtLabel = createGwtLabel();
        gwtLabel.addClassName("logo");
        logoWrapper.appendChild(gwtLabel);
        addCell(logoWrapper);
    }

    private Element createCell() {
        Element td = Document.get().createTDElement();
        td.setAttribute("align", "left");
        td.setAttribute("style", "vertical-align: " + (breadcrumbTableRow.hasChildNodes() ? "middle" : "top"));
        return td;
    }

    private Element createGwtLabel() {
        Element gwtLabel = Document.get().createDivElement();
        gwtLabel.addClassName(StyleConstantUtil.GWT_LABEL);
        return gwtLabel;
    }

}
