/*
 * Copyright 2017 - 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vaadin.spring.frontend.util;

import com.vaadin.server.FontIcon;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0
 *
 */
public final class FrontendIcons implements FontIcon {

    private int codepoint;

    private String fontFamily;

    public FrontendIcons(int codepoint, String fontFamily) {
        this.codepoint = codepoint;
        this.fontFamily = fontFamily;
    }

    public FrontendIcons() {
    }

    public void setCodepoint(int codepoint) {
        this.codepoint = codepoint;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    @Override
    public String getFontFamily() {
        return fontFamily;
    }

    @Override
    public int getCodepoint() {
        return codepoint;
    }

    @Override
    public String getHtml() {
        return null;
    }

    @Override
    public String getMIMEType() {
        return null;
    }
}
