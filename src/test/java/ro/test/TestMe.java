package ro.test;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FontIcon;
import com.vaadin.spring.frontend.shared.data.WebResourceReferenceDto;
import com.vaadin.spring.frontend.shared.html.HtmlConstantUtil;

import java.nio.charset.Charset;
import java.util.Locale;

public class TestMe {

    public static void main(String[] args) throws Exception {

        // Convert your representation of a char into a String object:
        String utf8char = "e2 80 99";
        String[] strNumbers = utf8char.split(" ");
        byte[] rawChars = new byte[strNumbers.length];
        int index = 0;
        for(String strNumber: strNumbers) {
            rawChars[index++] = (byte)(int)Integer.valueOf(strNumber, 16);
        }
        String utf16Char = new String(rawChars, Charset.forName("UTF-8"));

        // get the resulting characters (Java Strings are "encoded" in UTF16)
        int codePoint = utf16Char.charAt(0);
        if(Character.isSurrogate(utf16Char.charAt(0))) {
            codePoint = Character.toCodePoint(utf16Char.charAt(0), utf16Char.charAt(1));
        }
        System.out.println("code point: " + Integer.toHexString(codePoint));

        FontIcon fontIcon = VaadinIcons.USER;

        WebResourceReferenceDto ref = new WebResourceReferenceDto();
        ref.setRelationship("content-icon");
        ref.setFontFamily(fontIcon.getFontFamily());
        ref.setCodepoint(fontIcon.getCodepoint());

        HtmlConstantUtil.setContentIconHtml(ref);

        System.out.println(ref.getHtml());


        for (int n = 1; n < 16; n++)
            unity(n);

    }


    public static void unity(int n) {
        System.out.printf("%n%d: ", n);

        //all the way around the circle at even intervals
        for (double angle = 0; angle < 2 * Math.PI; angle += (2 * Math.PI) / n) {

            double real = Math.cos(angle); //real axis is the x axis

            if (Math.abs(real) < 1.0E-3)
                real = 0.0; //get rid of annoying sci notation

            double imag = Math.sin(angle); //imaginary axis is the y axis

            if (Math.abs(imag) < 1.0E-3)
                imag = 0.0;

            System.out.printf(Locale.US, "(%9f,%9f) ", real, imag);

            System.out.println("ce vrau eu top "+real * -50 + " ce vreau eu left " + imag * 50);
        }
    }
}
